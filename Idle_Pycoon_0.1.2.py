# Idle Pycoon
# A game by Aiden Whitlock, 11/2020
# Version 0.1.2

###################################################################################################################################
# CHANGELOG: added color, acronyms, and increasing difficulty levels/infinite gameplay! also fixed some grammar and spacing issues#
# Added wait command and smelters.                                                                                                #
###################################################################################################################################






# Color library
from colorama import Fore, Back, Style 

# Round counter
Round = 0

# Rescource definitions and starting counts
Wood = 45
Stone = 25
Ore = 20
Metal = 15
Ore_type_2 = 10
Metal_type_2 = 5
Food = 50

# Property stats (cost/production is in order: wood, stone, ore, metal, ore 2, metal 2, food)
# Here's the cool thing about how I programmed this game: you can
# change the production/price of a building, and even start wih some buildings!
# So you could, in theory, add a new building or a new resource(although good luck
# editing all the if statements!) to the game! plenty of mod support! Of course, 
# nobody's gonna play this really or mod it, but if you see this and you want to, then that's
# there for you! I suppose this is also kind of an advanced heello world?
# Discord bot conversion soon(ish(maybe(idk(ehh.. I'll see.))))! 


# Quantity owned
Location_1_owned = 0

# Purchase price (in order: wood, stone, ore, metal, ore 2, metal 2, food)
Location_1_cost =  [20, 20, 0, 0, 0, 0, 40]

# Production of items
Location_1_stats = [6, 8, 2, 0, 0, 0, 6]

Location_2_owned = 0
Location_2_cost =  [100, 175, 30, 0, 0, 0, 250]
Location_2_stats = [32, 24, 11, 0, 0, 0, 19]

Location_3_owned = 0
Location_3_cost =  [400, 1500, 1000, 175, 0, 0, 5000]
Location_3_stats = [14, 13, 12, 0, 1, 0, 500]

Location_4_owned = 0
Location_4_cost =  [2000, 2000, 2000, 2000, 100, 0, 71500]
Location_4_stats = [8, 100, 60, 0, 20, 0, 2800]

Location_5_owned = 0
Location_5_cost =  [500000, 570000, 55000, 0, 50050, 10000, 100000]
Location_5_stats = [5, 5, 50, 0, 500, 0, 10000]

Smelter_owned = 0
Smelter_cost =     [50, 50, 0, 0, 0, 0, 0]

Smelter_2_owned = 0
Smelter_2_cost =   [500, 500, 10, 15, 5, 5, 1]

Ore_smelt_cost = 5

Home_base_stats =  [5, 3, 10, 0, 0, 0, 12]

Goal = 1000000000000
Time_Goal = 10000

Combined_stats_wood = 0
Combined_stats_stone = 0
Combined_stats_ore = 0
Combined_stats_metal = 0
Combined_stats_ore2 = 0
Combined_stats_metal2 = 0
Combined_stats_food = 0
print("\n"* 100)





print(Fore.MAGENTA, Back.BLACK, Style.BRIGHT, "Welcome to IDLE PYCOON! The hottest new crapcode jumble from some random 14 year old!")
print("Your goal is to make 1,000,000,000,000 Metal type two in 10000 days!")
print("Here are some tips:")
print("1: Use <help> to see how much more metal 2 you need to win!")
print("2: When buying buildings, you can use the \"all\" or \"half\" command instead of entering a 12-digit number repeatedly!")
print("3: When you are late game, don't forget to go back and buy a ton of building 1's, 2's, and 3's! Remember, A BUILDING 3 MAKES 1 ORE 2 PER TURN!", Style.RESET_ALL)

while True:
        try:
                        
                # logic for production math starts here...

                # reset display values
                Combined_stats_wood = 0
                Combined_stats_stone = 0
                Combined_stats_ore = 0
                Combined_stats_metal = 0
                Combined_stats_ore2 = 0
                Combined_stats_metal2 = 0
                Combined_stats_food = 0

                # Re-calculate displayed values(this method of resetting and recalculating 
                # the production totals each turn was simpler than increasing a number, since
                # that would be much more code and also prevents bugs where you could just set
                # a combined stats value to 9999999 via console and get free stuff. Quite 
                # usefull for debug, but for obvios reasons patched out in the actuall game. 
                # Hope that made sense!)

                if Location_1_owned >= 1:
                        Combined_stats_wood += (Location_1_stats[0] * Location_1_owned)
                        Combined_stats_stone += (Location_1_stats[1] * Location_1_owned)
                        Combined_stats_ore += (Location_1_stats[2] * Location_1_owned)
                        Combined_stats_metal += (Location_1_stats[3] * Location_1_owned)
                        Combined_stats_ore2 += (Location_1_stats[4] * Location_1_owned)
                        Combined_stats_metal2 += (Location_1_stats[5] * Location_1_owned)
                        Combined_stats_food += (Location_1_stats[6] * Location_1_owned)
                if Location_2_owned >= 1:
                        Combined_stats_wood += (Location_2_stats[0] * Location_2_owned)
                        Combined_stats_stone += (Location_2_stats[1] * Location_2_owned)
                        Combined_stats_ore += (Location_2_stats[2] * Location_2_owned)
                        Combined_stats_metal += (Location_2_stats[3] * Location_2_owned)
                        Combined_stats_ore2 += (Location_2_stats[4] * Location_2_owned)
                        Combined_stats_metal2 += (Location_2_stats[5] * Location_2_owned)
                        Combined_stats_food += (Location_2_stats[6] * Location_2_owned)
                if Location_3_owned >= 1:
                        Combined_stats_wood += (Location_3_stats[0] * Location_3_owned)
                        Combined_stats_stone += (Location_3_stats[1] * Location_3_owned)
                        Combined_stats_ore += (Location_3_stats[2] * Location_3_owned)
                        Combined_stats_metal += (Location_3_stats[3] * Location_3_owned)
                        Combined_stats_ore2 += (Location_3_stats[4] * Location_3_owned)
                        Combined_stats_metal2 += (Location_3_stats[5] * Location_3_owned)
                        Combined_stats_food += (Location_3_stats[6] * Location_3_owned)
                if Location_4_owned >= 1:
                        Combined_stats_wood += (Location_4_stats[0] * Location_4_owned)
                        Combined_stats_stone += (Location_4_stats[1] * Location_4_owned)
                        Combined_stats_ore += (Location_4_stats[2] * Location_4_owned)
                        Combined_stats_metal += (Location_4_stats[3] * Location_4_owned)
                        Combined_stats_ore2 += (Location_4_stats[4] * Location_4_owned)
                        Combined_stats_metal2 += (Location_4_stats[5] * Location_4_owned)
                        Combined_stats_food += (Location_4_stats[6] * Location_4_owned)
                if Location_5_owned >= 1:
                        Combined_stats_wood += (Location_5_stats[0] * Location_5_owned)
                        Combined_stats_stone += (Location_5_stats[1] * Location_5_owned)
                        Combined_stats_ore += (Location_5_stats[2] * Location_5_owned)
                        Combined_stats_metal += (Location_5_stats[3] * Location_5_owned)
                        Combined_stats_ore2 += (Location_5_stats[4] * Location_5_owned)
                        Combined_stats_metal2 += (Location_5_stats[5] * Location_5_owned)
                        Combined_stats_food += (Location_5_stats[6] * Location_5_owned)
                if Smelter_owned >= 1:
                        Combined_stats_ore -= Smelter_owned * 5
                        Combined_stats_metal += Smelter_owned
                if Smelter_2_owned >= 1:
                        Combined_stats_ore2 -= Smelter_2_owned * 5
                        Combined_stats_metal2 += Smelter_2_owned
                Combined_stats_wood += Home_base_stats[0]
                Combined_stats_stone += Home_base_stats[1]
                Combined_stats_ore += Home_base_stats[2]
                Combined_stats_metal += Home_base_stats[3]
                Combined_stats_ore2 += Home_base_stats[4]
                Combined_stats_metal2 += Home_base_stats[5] 
                Combined_stats_food += Home_base_stats[6]

                # ...and ends here.

                # Main header display code
                print("\n")
                print("\n")
                print(Fore.GREEN, "Current total production stats: Wood: +", Combined_stats_wood, ", Stone: +", Combined_stats_stone, ", Ore: +", Combined_stats_ore, ", Metal: +", Combined_stats_metal, ", Ore 2: +", Combined_stats_ore2, ", Metal 2: +", Combined_stats_metal2, ", Food: +", Combined_stats_food, Style.RESET_ALL)
                print("\n")
                print(Fore.BLUE, "Day", Round, Style.RESET_ALL, ": Total resource counts are:", Fore.YELLOW, "Wood:", Wood, ",  Stone:", Stone, ",  Ore:", Ore, ",  Metal:", Metal, ",  Ore type 2:", Ore_type_2, ",  Metal type 2:", Metal_type_2, ",  Food:", Food, Style.RESET_ALL)
                print()
                print(Style.BRIGHT, "Type \"help\" for help\commands list, or \"stop\" to stop. Hit return to advance turn.")
                print(Style.RESET_ALL + Fore.CYAN, "\n")
                Input = input("> " + Fore.MAGENTA)
                print(Style.RESET_ALL, "\n")
                print("\n")

                # All of the input logic processing
                if Input == "help" or Input == "h":
                        # Help: fairly self explanatory
                        print("\n" + Style.BRIGHT)
                        print("Idle Pycoon v0.1.2, 11/8/20. CHANGELOG: added color, acronyms, and infinite gameplay! Added \"wait\" and smelters.")
                        print("Availible commands are:")
                        print("\n")
                        print("> buy <1, 2, 3, 4, 5> (acronyms: build 1, b1, etc.")
                        print("> craft <metal, metal 2, smelter> (acronyms: c1, cm1, cs, etc.)")
                        print("> stats <1, 2, 3, 4, 5> (acronyms: s1, s2, etc.)")
                        print("> reset (will ask you to confirm)")
                        print("> wait (acronyms: w)")
                        print("\n")
                        print("Currently, your goal is to make", Goal, "metal type 2 in", Time_Goal, "days.")
                        print("You need", Goal - Metal_type_2, "more metal type 2 to reach that goal. ")
                        print("When you reach this goal, these numbers will change and your stats will reset! Check back to this menu to find out how much!")
                        print("\n")
                        print("Here are some tips:")
                        print("1: Use <help> to see how much more metal 2 you need to win!")
                        print("2: When buying buildings, you can use the \"all\" or \"half\" command instead of entering a 12-digit number repeatedly!")
                        print("3: When you are late game, don't forget to go back and buy a ton of building 1's, 2's, and 3's! Remember, A BUILDING 3 MAKES 1 ORE TYPE 2 PER TURN!!!", Style.RESET_ALL)

                elif Input == "stop":
                        # Do I even need to annotate this?
                        Input = input("Are you sure? type YES to confirm, anything else cancels!!!")
                        if Input == "YES":
                                break
                        else:
                                print("Cancelled!")
                elif Input == "buy 1" or Input == "b1" or Input == "build 1":
                        print("\n")
                        # I'm gonna be honest here: Don't try to understand this 
                        # huge, somewhat inefficient, semi-optimized, absolute JUMBLE
                        # of kludged together code, unless you want your head to 
                        # explode. What should have been a simple task of calculating 
                        # how many of a building you can buy ended up takin 5 hours of 
                        # debugging and programming over the course of two days. You have
                        # been warned. I have a good reason I'm not annotating this.
                        # The good news is, if you added a new resource, all you have to
                        # do is add the location cost index for that resource, and the
                        # main count for that resource, IN THAT ORDER. Refer to previous
                        # entries in the list for examples. 


                        Location_1_logicB = []
                        Location_1_logicA = [Location_1_cost[0], Wood, Location_1_cost[1], Stone, Location_1_cost[2], Ore, Location_1_cost[3], Metal, Location_1_cost[4], Ore_type_2, Location_1_cost[5], Metal_type_2, Location_1_cost[6], Food]
                        for i in range(Location_1_cost.count(0)):
                                del Location_1_logicA[Location_1_cost.index(0) * 2]
                                del Location_1_logicA[Location_1_cost.index(0) * 2]
                        for i in range(int(len(Location_1_logicA) / 2)):
                                Location_1_logicB.append(int((Location_1_logicA[((i - 1) * 2) + 1] - (Location_1_logicA[((i - 1) * 2) + 1] % Location_1_logicA[((i - 1) * 2)])) / Location_1_logicA[((i - 1) * 2)]))
                        Location_1_logicB.sort()
                        print("You can buy", Fore.GREEN, Location_1_logicB[0], Style.RESET_ALL, "of this building.") 

                        # End of crappy code

                        
                        # Ok, so this one you can kinda understand, it's mostly
                        # just brute force of many "if resource is grater than cost 
                        # times the amount you are purchasing" statements strung 
                        # together to make a very large conditional. The subtracting
                        # of the cost is also very easy to understand, enough so
                        # that I'm not going to annotate it. Adding a new resource
                        # should be fairly simple, just add another AND statement. 
        
                        amount = input(Fore.CYAN + "How many do you want to buy? " + Fore.MAGENTA)
                        if amount == "all":
                                print(Style.RESET_ALL + Style.BRIGHT + "Purchasing all!" + Style.RESET_ALL)
                                amount = Location_1_logicB[0]
                        elif amount == "half":
                                print(Style.RESET_ALL + Style.BRIGHT + "Purchasing half!" + Style.RESET_ALL)
                                amount = int(Location_1_logicB[0] / 2)
                        else:
                                amount = int(amount)
                        if Wood >= Location_1_cost[0] * amount and Stone >= Location_1_cost[1] * amount and Ore >= Location_1_cost[2] * amount and Metal >= Location_1_cost[3] * amount and Ore_type_2 >= Location_1_cost[4] * amount and Metal_type_2 >= Location_1_cost[5] * amount and Food >= Location_1_cost[6] * amount and amount >= 0:
                                Wood -= (Location_1_cost[0] * amount)
                                Stone -= (Location_1_cost[1] * amount)
                                Ore -= (Location_1_cost[2] * amount)
                                Metal -= (Location_1_cost[3] * amount) 
                                Ore_type_2 -= (Location_1_cost[4] * amount) 
                                Metal_type_2 -= (Location_1_cost[5] * amount)
                                Food -= (Location_1_cost[6] * amount)
                                Location_1_owned += (1 * amount)
                                print("\n")
                                print("Purchased ", amount, "Building 1")
                                print("\n")
                        elif amount >= 0: 
                                # And if that massive conditional is false,
                                # then they must not have enough.
                                print("\n")
                                print("Not enough Resources!!! You need: Wood:", Location_1_cost[0] * amount, ", Stone:", Location_1_cost[1] * amount, ", Ore:", Location_1_cost[2] * amount, ", Metal:", Location_1_cost[3] * amount, ", Ore type 2:", Location_1_cost[4] * amount, ", Metal type 2:", Location_1_cost[5] * amount, ", Food:", Location_1_cost[6] * amount, "to buy ", amount, "of those!!!")
                                print("\n")
                        else:
                                print("You can't buy a negative amount!!! Nice try.")
                elif Input == "buy 2" or Input == "b2" or Input == "build 2":
                        print("\n")
                        Location_2_logicB = []
                        Location_2_logicA = [Location_2_cost[0], Wood, Location_2_cost[1], Stone, Location_2_cost[2], Ore, Location_2_cost[3], Metal, Location_2_cost[4], Ore_type_2, Location_2_cost[5], Metal_type_2, Location_2_cost[6], Food]
                        for i in range(Location_2_cost.count(0)):
                                del Location_2_logicA[Location_2_cost.index(0) * 2]
                                del Location_2_logicA[Location_2_cost.index(0) * 2]
                        for i in range(int(len(Location_2_logicA) / 2)):
                                Location_2_logicB.append(int((Location_2_logicA[((i - 1) * 2) + 1] - (Location_2_logicA[((i - 1) * 2) + 1] % Location_2_logicA[((i - 1) * 2)])) / Location_2_logicA[((i - 1) * 2)]))
                        Location_2_logicB.sort()
                        print("You can buy", Fore.GREEN, Location_2_logicB[0], Style.RESET_ALL, "of this building.") 
                        amount = input(Fore.CYAN + "How many do you want to buy? " + Fore.MAGENTA)
                        if amount == "all":
                                print(Style.RESET_ALL + Style.BRIGHT + "Purchasing all!" + Style.RESET_ALL)
                                amount = Location_2_logicB[0]
                        elif amount == "half":
                                print(Style.RESET_ALL + Style.BRIGHT + "Purchasing half!" + Style.RESET_ALL)
                                amount = int(Location_2_logicB[0] / 2)
                        else:
                                amount = int(amount)
                        if Wood >= Location_2_cost[0] * amount and Stone >= Location_2_cost[1] * amount and Ore >= Location_2_cost[2] * amount and Metal >= Location_2_cost[3] * amount and Ore_type_2 >= Location_2_cost[4] * amount and Metal_type_2 >= Location_2_cost[5] * amount and Food >= Location_2_cost[6] * amount and amount >= 0:
                                Wood -= (Location_2_cost[0] * amount)
                                Stone -= (Location_2_cost[1] * amount)
                                Ore -= (Location_2_cost[2] * amount)
                                Metal -= (Location_2_cost[3] * amount) 
                                Ore_type_2 -= (Location_2_cost[4] * amount) 
                                Metal_type_2 -= (Location_2_cost[5] * amount)
                                Food -= (Location_2_cost[6] * amount)
                                Location_2_owned += (1 * amount)
                                print("\n")
                                print("Purchased ", amount, "Building 2")
                                print("\n")
                        elif amount >= 0: 
                                print("\n")
                                print("Not enough Rescources!!! You need: Wood:", Location_2_cost[0] * amount, ", Stone:", Location_2_cost[1] * amount, ", Ore:", Location_2_cost[2] * amount, ", Metal:", Location_2_cost[3] * amount, ", Ore type 2:", Location_2_cost[4] * amount, ", Metal type 2:", Location_2_cost[5] * amount, ", Food:", Location_2_cost[6] * amount, "to buy ", amount, "of those!!!")
                                print("\n")
                        else:
                                print("You can't buy a negative amount!!! Nice try.")
                elif Input == "buy 3" or Input == "b3" or Input == "build 3":
                        print("\n")
                        Location_3_logicB = []
                        Location_3_logicA = [Location_3_cost[0], Wood, Location_3_cost[1], Stone, Location_3_cost[2], Ore, Location_3_cost[3], Metal, Location_3_cost[4], Ore_type_2, Location_3_cost[5], Metal_type_2, Location_3_cost[6], Food]
                        for i in range(Location_3_cost.count(0)):
                                del Location_3_logicA[Location_3_cost.index(0) * 2]
                                del Location_3_logicA[Location_3_cost.index(0) * 2]
                        for i in range(int(len(Location_3_logicA) / 2)):
                                Location_3_logicB.append(int((Location_3_logicA[((i - 1) * 2) + 1] - (Location_3_logicA[((i - 1) * 2) + 1] % Location_3_logicA[((i - 1) * 2)])) / Location_3_logicA[((i - 1) * 2)]))
                        Location_3_logicB.sort()
                        print("You can buy", Fore.GREEN, Location_3_logicB[0], Style.RESET_ALL, "of this building.")
                        amount = input(Fore.CYAN + "How many do you want to buy? " + Fore.MAGENTA)
                        if amount == "all":
                                print(Style.RESET_ALL + Style.BRIGHT + "Purchasing all!" + Style.RESET_ALL)
                                amount = Location_3_logicB[0]
                        elif amount == "half":
                                print(Style.RESET_ALL + Style.BRIGHT + "Purchasing half!" + Style.RESET_ALL)
                                amount = int(Location_3_logicB[0] / 2)
                        else:
                                amount = int(amount)
                        if Wood >= Location_3_cost[0] * amount and Stone >= Location_3_cost[1] * amount and Ore >= Location_3_cost[2] * amount and Metal >= Location_3_cost[3] * amount and Ore_type_2 >= Location_3_cost[4] * amount and Metal_type_2 >= Location_3_cost[5] * amount and Food >= Location_3_cost[6] * amount and amount >= 0:
                                Wood -= (Location_3_cost[0] * amount)
                                Stone -= (Location_3_cost[1] * amount)
                                Ore -= (Location_3_cost[2] * amount)
                                Metal -= (Location_3_cost[3] * amount) 
                                Ore_type_2 -= (Location_3_cost[4] * amount) 
                                Metal_type_2 -= (Location_3_cost[5] * amount)
                                Food -= (Location_3_cost[6] * amount)
                                Location_3_owned += (1 * amount)
                                print("\n")
                                print("Purchased ", amount, "Building 3")
                                print("\n")
                        elif amount >= 0: 
                                print("\n")
                                print("Not enough Rescources!!! You need: Wood:", Location_3_cost[0] * amount, ", Stone:", Location_3_cost[1] * amount, ", Ore:", Location_3_cost[2] * amount, ", Metal:", Location_3_cost[3] * amount, ", Ore type 2:", Location_3_cost[4] * amount, ", Metal type 2:", Location_3_cost[5] * amount, ", Food:", Location_3_cost[6] * amount, "to buy ", amount, "of those!!!")
                                print("\n")
                        else:
                                print("You can't buy a negative amount!!! Nice try.")
                elif Input == "buy 4" or Input == "b4" or Input == "build 4":
                        print("\n")
                        Location_4_logicB = []
                        Location_4_logicA = [Location_4_cost[0], Wood, Location_4_cost[1], Stone, Location_4_cost[2], Ore, Location_4_cost[3], Metal, Location_4_cost[4], Ore_type_2, Location_4_cost[5], Metal_type_2, Location_4_cost[6], Food]
                        for i in range(Location_4_cost.count(0)):
                                del Location_4_logicA[Location_4_cost.index(0) * 2]
                                del Location_4_logicA[Location_4_cost.index(0) * 2]
                        for i in range(int(len(Location_4_logicA) / 2)):
                                Location_4_logicB.append(int((Location_4_logicA[((i - 1) * 2) + 1] - (Location_4_logicA[((i - 1) * 2) + 1] % Location_4_logicA[((i - 1) * 2)])) / Location_4_logicA[((i - 1) * 2)]))
                        Location_4_logicB.sort()
                        print("You can buy", Fore.GREEN, Location_4_logicB[0], Style.RESET_ALL, "of this building.")
                        amount = input(Fore.CYAN + "How many do you want to buy? " + Fore.MAGENTA)
                        if amount == "all":
                                print(Style.RESET_ALL + Style.BRIGHT + "Purchasing all!" + Style.RESET_ALL)
                                amount = Location_4_logicB[0]
                        elif amount == "half":
                                print(Style.RESET_ALL + Style.BRIGHT + "Purchasing half!" + Style.RESET_ALL)
                                amount = int(Location_4_logicB[0] / 2)
                        else:
                                amount = int(amount)
                        if Wood >= Location_4_cost[0] * amount and Stone >= Location_4_cost[1] * amount and Ore >= Location_4_cost[2] * amount and Metal >= Location_4_cost[3] * amount and Ore_type_2 >= Location_4_cost[4] * amount and Metal_type_2 >= Location_4_cost[5] * amount and Food >= Location_4_cost[6] * amount:
                                Wood -= (Location_4_cost[0] * amount)
                                Stone -= (Location_4_cost[1] * amount)
                                Ore -= (Location_4_cost[2] * amount)
                                Metal -= (Location_4_cost[3] * amount) 
                                Ore_type_2 -= (Location_4_cost[4] * amount) 
                                Metal_type_2 -= (Location_4_cost[5] * amount)
                                Food -= (Location_4_cost[6] * amount)
                                Location_4_owned += (1 * amount)
                                print("\n")
                                print("Purchased ", amount, "Building 4")
                                print("\n")
                        else: 
                                print("\n")
                                print("Not enough Rescources!!! You need: Wood:", Location_4_cost[0] * amount, ", Stone:", Location_4_cost[1] * amount, ", Ore:", Location_4_cost[2] * amount, ", Metal:", Location_4_cost[3] * amount, ", Ore type 2:", Location_4_cost[4] * amount, ", Metal type 2:", Location_4_cost[5] * amount, ", Food:", Location_4_cost[6] * amount, "to buy ", amount, "of those!!!")
                                print("\n")
                elif Input == "buy 5" or Input == "b5" or Input == "build 5":
                        print("\n")
                        Location_5_logicB = []
                        Location_5_logicA = [Location_5_cost[0], Wood, Location_5_cost[1], Stone, Location_5_cost[2], Ore, Location_5_cost[3], Metal, Location_5_cost[4], Ore_type_2, Location_5_cost[5], Metal_type_2, Location_5_cost[6], Food]
                        for i in range(Location_5_cost.count(0)):
                                del Location_5_logicA[Location_5_cost.index(0) * 2]
                                del Location_5_logicA[Location_5_cost.index(0) * 2]
                        for i in range(int(len(Location_5_logicA) / 2)):
                                Location_5_logicB.append(int((Location_5_logicA[((i - 1) * 2) + 1] - (Location_5_logicA[((i - 1) * 2) + 1] % Location_5_logicA[((i - 1) * 2)])) / Location_5_logicA[((i - 1) * 2)]))
                        Location_5_logicB.sort()
                        print("You can buy", Fore.GREEN, Location_5_logicB[0], Style.RESET_ALL, "of this building.")
                        amount = input(Fore.CYAN + "How many do you want to buy? " + Fore.MAGENTA)
                        if amount == "all":
                                print(Style.RESET_ALL + Style.BRIGHT + "Purchasing all!" + Style.RESET_ALL)
                                amount = Location_5_logicB[0]
                        elif amount == "half":
                                print(Style.RESET_ALL + Style.BRIGHT + "Purchasing half!" + Style.RESET_ALL)
                                amount = int(Location_5_logicB[0] / 2)
                        else:
                                amount = int(amount)
                        if Wood >= Location_5_cost[0] * amount and Stone >= Location_5_cost[1] * amount and Ore >= Location_5_cost[2] * amount and Metal >= Location_5_cost[3] * amount and Ore_type_2 >= Location_5_cost[4] * amount and Metal_type_2 >= Location_5_cost[5] * amount and Food >= Location_5_cost[6] * amount:
                                Wood -= (Location_5_cost[0] * amount)
                                Stone -= (Location_5_cost[1] * amount)
                                Ore -= (Location_5_cost[2] * amount)
                                Metal -= (Location_5_cost[3] * amount) 
                                Ore_type_2 -= (Location_5_cost[4] * amount) 
                                Metal_type_2 -= (Location_5_cost[5] * amount)
                                Food -= (Location_5_cost[6] * amount)
                                Location_5_owned += (1 * amount)
                                print("\n")
                                print("Purchased ", amount, "Building 5")
                                print("\n")
                        else: 
                                print("\n")
                                print("Not enough Rescources!!! You need: Wood:", Location_5_cost[0] * amount, ", Stone:", Location_5_cost[1] * amount, ", Ore:", Location_5_cost[2] * amount, ", Metal:", Location_5_cost[3] * amount, ", Ore type 2:", Location_5_cost[4] * amount, ", Metal type 2:", Location_5_cost[5] * amount, ", Food:", Location_5_cost[6] * amount, "to buy ", amount, "of those!!!")
                                print("\n")
                elif Input == "stats 1" or Input == "s1":
                        # Very simple. It prints some basic stats, and is linked to
                        # the main variables. You will still have to add the stat and
                        # cost index, but other than that, it's very simple to add something. 
                        print("\n")
                        print("You own:", Fore.YELLOW, Location_1_owned, Style.RESET_ALL, "of this building.")
                        print(Fore.GREEN, "\n")
                        print("Production stats: Wood: ", Location_1_stats[0], ", Stone:", Location_1_stats[1], ", Ore:", Location_1_stats[2], ", Metal:", Location_1_stats[3], ", Ore 2:", Location_1_stats[4], ", Metal 2:", Location_1_stats[5], ", Food:", Location_1_stats[6])
                        print(Fore.RED, "\n")
                        print("Building price: Wood: ", Location_1_cost[0], ", Stone:", Location_1_cost[1], ", Ore:", Location_1_cost[2], ", Metal:", Location_1_cost[3], ", Ore 2:", Location_1_cost[4], ", Metal 2:", Location_1_cost[5], ", Food:", Location_1_cost[6])
                        print(Style.RESET_ALL, "\n")
                elif Input == "stats 2" or Input == "s2":
                        print("\n")
                        print("You own:", Fore.YELLOW, Location_2_owned, Style.RESET_ALL, "of this building.")
                        print(Fore.GREEN, "\n")
                        print("Production stats: Wood: ", Location_2_stats[0], ", Stone:", Location_2_stats[1], ", Ore:", Location_2_stats[2], ", Metal:", Location_2_stats[3], ", Ore 2:", Location_2_stats[4], ", Metal 2:", Location_2_stats[5], ", Food:", Location_2_stats[6])
                        print(Fore.RED, "\n")
                        print("Building price: Wood: ", Location_2_cost[0], ", Stone:", Location_2_cost[1], ", Ore:", Location_2_cost[2], ", Metal:", Location_2_cost[3], ", Ore 2:", Location_2_cost[4], ", Metal 2:", Location_2_cost[5], ", Food:", Location_2_cost[6])
                        print(Style.RESET_ALL, "\n")
                elif Input == "stats 3" or Input == "s3":
                        print("\n")
                        print("You own:", Fore.YELLOW, Location_3_owned, Style.RESET_ALL, "of this building.")
                        print(Fore.GREEN, "\n")
                        print("Production stats: Wood: ", Location_3_stats[0], ", Stone:", Location_3_stats[1], ", Ore:", Location_3_stats[2], ", Metal:", Location_3_stats[3], ", Ore 2:", Location_3_stats[4], ", Metal 2:", Location_3_stats[5], ", Food:", Location_3_stats[6])
                        print(Fore.RED, "\n")
                        print("Building price: Wood: ", Location_3_cost[0], ", Stone:", Location_3_cost[1], ", Ore:", Location_3_cost[2], ", Metal:", Location_3_cost[3], ", Ore 2:", Location_3_cost[4], ", Metal 2:", Location_3_cost[5], ", Food:", Location_3_cost[6])
                        print(Style.RESET_ALL, "\n")
                elif Input == "stats 4" or Input == "s4":
                        print("\n")
                        print("You own:", Fore.YELLOW, Location_4_owned, Style.RESET_ALL, "of this building.")
                        print(Fore.GREEN, "\n")
                        print("Production stats: Wood: ", Location_4_stats[0], ", Stone:", Location_4_stats[1], ", Ore:", Location_4_stats[2], ", Metal:", Location_4_stats[3], ", Ore 2:", Location_4_stats[4], ", Metal 2:", Location_4_stats[5], ", Food:", Location_4_stats[6])
                        print(Fore.RED, "\n")
                        print("Building price: Wood: ", Location_4_cost[0], ", Stone:", Location_4_cost[1], ", Ore:", Location_4_cost[2], ", Metal:", Location_4_cost[3], ", Ore 2:", Location_4_cost[4], ", Metal 2:", Location_4_cost[5], ", Food:", Location_4_cost[6])
                        print(Style.RESET_ALL, "\n")
                elif Input == "stats 5" or Input == "s5":
                        print("\n")
                        print("You own:", Fore.YELLOW, Location_5_owned, Style.RESET_ALL, "of this building.")
                        print(Fore.GREEN, "\n")
                        print("Production stats: Wood: ", Location_5_stats[0], ", Stone:", Location_5_stats[1], ", Ore:", Location_5_stats[2], ", Metal:", Location_5_stats[3], ", Ore 2:", Location_5_stats[4], ", Metal 2:", Location_5_stats[5], ", Food:", Location_5_stats[6]) 
                        print(Fore.RED, "\n")
                        print("Building price: Wood: ", Location_5_cost[0], ", Stone:", Location_5_cost[1], ", Ore:", Location_5_cost[2], ", Metal:", Location_5_cost[3], ", Ore 2:", Location_5_cost[4], ", Metal 2:", Location_5_cost[5], ", Food:", Location_5_cost[6])
                        print(Style.RESET_ALL, "\n")
                elif Input == "reset":
                        # Obviously, we need a confirmation check...
                        Input = input(Fore.CYAN + "Are you sure? type YES to confirm, anything else cancels!!!" + Fore.MAGENTA)
                        if Input == "YES":
                                # ...but other than that, it just zeros out the counts of everything.
                                # You will need to add the resource counts and building ID's, 
                                # but otherwise, it's as simple as pie.
                                Wood = 0
                                Stone = 0
                                Ore = 0
                                Metal = 0
                                Ore_type_2 = 0
                                Metal_type_2 = 0
                                Food = 0
                                Location_1_owned = 0
                                Location_2_owned = 0
                                Location_3_owned = 0
                                Location_4_owned = 0
                                Location_5_owned = 0
                                Round = 0
                        else:
                                print(Fore.RED, "Canceled!", Style.RESET_ALL)
                elif Input == "craft metal" or Input == "c1" or Input == "cm1":
                        # So, the crafting uses the same logic as the building count, but way simpler.
                        # In fact, the building count code was actually derived from this very code!
                        print("You can craft", Fore.GREEN, int((Ore - (Ore % Ore_smelt_cost)) / Ore_smelt_cost), Style.RESET_ALL, "metal.")
                        Input = input(Fore.CYAN + "How many do you want to craft? " + Fore.MAGENTA)
                        print(Style.RESET_ALL)
                        if Input == "all":
                                print(Style.RESET_ALL + Style.BRIGHT + "Purchasing all!" + Style.RESET_ALL)
                                Input = int((Ore - (Ore % 5)) / 5)
                        elif amount == "half":
                                print(Style.RESET_ALL + Style.BRIGHT + "Purchasing half!" + Style.RESET_ALL)
                                amount = int(int((Ore - (Ore % Ore_smelt_cost)) / Ore_smelt_cost) / 2)
                        else:
                                Input = int(Input)
                        if Ore >= (Input * Ore_smelt_cost):
                                Ore -= (Input * Ore_smelt_cost)
                                Metal += Input
                                print("\n")
                                print("Crafted +", Fore.GREEN, Input, Style.RESET_ALL, "Metal for -", Fore.RED, (Input * Ore_smelt_cost), Style.RESET_ALL, "Ore!")
                                print("\n")
                        elif Ore < (Input * Ore_smelt_cost):
                                print("\n")
                                print(Fore.RED, "Not enough ore! You need", (Input * Ore_smelt_cost), "ore to craft that!", Style.RESET_ALL)
                                print("\n")
                        else:
                                print("\n")
                                print("Invalid commmand or argument!")
                                print("\n")
                elif Input == "craft metal 2" or Input == "c2" or Input == "cm2":
                        print("Remember, your goal is to make ", Goal, "metal type 2 in", Time_Goal, "days.")
                        print("You need     ", Fore.YELLOW, Goal - Metal_type_2, Style.RESET_ALL, "more metal type 2 to reach that goal.")
                        print("You can craft", Fore.GREEN, int((Ore_type_2 - (Ore_type_2 % Ore_smelt_cost)) / Ore_smelt_cost), Style.RESET_ALL, "metal type 2.")
                        Input = input(Fore.CYAN + "How many do you want to craft? " + Fore.MAGENTA)
                        print(Style.RESET_ALL)
                        if Input == "all":
                                print(Style.RESET_ALL + Style.BRIGHT + "Purchasing all!" + Style.RESET_ALL)
                                Input = int((Ore_type_2 - (Ore_type_2 % 5)) / 5)
                        elif amount == "half":
                                print(Style.RESET_ALL + Style.BRIGHT + "Purchasing half!" + Style.RESET_ALL)
                                amount = int(int((Ore_type_2 - (Ore_type_2 % Ore_smelt_cost)) / Ore_smelt_cost) / 2)
                        else:
                                Input = int(Input)
                        if Ore_type_2 >= (Input * 5):
                                Ore_type_2 -= (Input * 5)
                                Metal_type_2 += Input
                                print("\n")
                                print("Crafted", Fore.GREEN, Input, Style.RESET_ALL, "Metal type 2 for", Fore.RED, (Input * 5), Style.RESET_ALL, "Ore type 2!")
                                print("\n")
                        elif Ore_type_2 < (Input * 5):
                                print("\n")
                                print(Fore.RED, "Not enough ore type 2! You need", (Input * 5), "ore type 2 to craft that!", Style.RESET_ALL)
                                print("\n")
                        else:
                                print("\n")
                                print("Invalid commmand or argument!")
                                print("\n")
                elif Input == "":
                        print("\n"* 55)
                        Wood += Combined_stats_wood
                        Stone += Combined_stats_stone
                        Ore += Combined_stats_ore
                        Metal += Combined_stats_metal
                        Ore_type_2 += Combined_stats_ore2
                        Metal_type_2 += Combined_stats_metal2
                        Food += Combined_stats_food
                        Round += 1
                elif Input == "wait" or Input == "w":
                        Input = int(input(Fore.CYAN + "How long to wait? " + Fore.MAGENTA))
                        print("\n"* 55)
                        Wood += int(Combined_stats_wood * Input)
                        Stone += int(Combined_stats_stone * Input)
                        Ore += int(Combined_stats_ore * Input)
                        Metal += int(Combined_stats_metal * Input)
                        Ore_type_2 += int(Combined_stats_ore2 * Input)
                        Metal_type_2 += int(Combined_stats_metal2 * Input)
                        Food += int(Combined_stats_food * Input)
                        Round += int(Input)
                        print("Waited", Input, "Rounds!")
                elif Input == "craft smelter" or Input == "cs":
                        print("\n")
                        Smelter_logicB = []
                        Smelter_logicA = [Smelter_cost[0], Wood, Smelter_cost[1], Stone, Smelter_cost[2], Ore, Smelter_cost[3], Metal, Smelter_cost[4], Ore_type_2, Smelter_cost[5], Metal_type_2, Smelter_cost[6], Food, Ore_smelt_cost, Combined_stats_ore]
                        for i in range(Smelter_cost.count(0)):
                                del Smelter_logicA[Smelter_cost.index(0) * 2]
                                del Smelter_logicA[Smelter_cost.index(0) * 2]
                        for i in range(int(len(Smelter_logicA) / 2)):
                                Smelter_logicB.append(int((Smelter_logicA[((i - 1) * 2) + 1] - (Smelter_logicA[((i - 1) * 2) + 1] % Smelter_logicA[((i - 1) * 2)])) / Smelter_logicA[((i - 1) * 2)]))
                        Smelter_logicB.sort()
                        print("You can buy", Fore.GREEN, Smelter_logicB[0], Style.RESET_ALL, "of this building.") 
                        amount = input(Fore.CYAN + "How many do you want to buy? " + Fore.MAGENTA)
                        if amount == "all":
                                print(Style.RESET_ALL + Style.BRIGHT + "Purchasing all!" + Style.RESET_ALL)
                                amount = Smelter_logicB[0]
                        elif amount == "half":
                                print(Style.RESET_ALL + Style.BRIGHT + "Purchasing half!" + Style.RESET_ALL)
                                amount = int(Smelter_logicB[0] / 2)
                        else:
                                amount = int(amount)
                        if Wood >= Smelter_cost[0] * amount and Stone >= Smelter_cost[1] * amount and Ore >= Smelter_cost[2] * amount and Metal >= Smelter_cost[3] * amount and Ore_type_2 >= Smelter_cost[4] * amount and Metal_type_2 >= Smelter_cost[5] * amount and Food >= Smelter_cost[6] * amount and amount >= 0 and Combined_stats_ore >= Ore_smelt_cost * amount:
                                Wood -= (Smelter_cost[0] * amount)
                                Stone -= (Smelter_cost[1] * amount)
                                Ore -= (Smelter_cost[2] * amount)
                                Metal -= (Smelter_cost[3] * amount) 
                                Ore_type_2 -= (Smelter_cost[4] * amount) 
                                Metal_type_2 -= (Smelter_cost[5] * amount)
                                Food -= (Smelter_cost[6] * amount)
                                Smelter_owned += (1 * amount)
                                print("\n")
                                print("Purchased ", amount, "Smelters!")
                                print("\n")
                        elif amount >= 0 and Combined_stats_ore >= Ore_smelt_cost * amount: 
                                print("\n")
                                print("Not enough Rescources!!! You need: Wood:", Smelter_cost[0] * amount, ", Stone:", Smelter_cost[1] * amount, ", Ore:", Smelter_cost[2] * amount, ", Metal:", Smelter_cost[3] * amount, ", Ore type 2:", Smelter_cost[4] * amount, ", Metal type 2:", Smelter_cost[5] * amount, ", Food:", Smelter_cost[6] * amount, "to buy ", amount, "of those!!!")
                                print("\n")
                        elif amount >= 0 and Combined_stats_ore < Ore_smelt_cost * amount:
                                print("\n")
                                print("You aren't producing enough ore to supply that many smelters!")
                                print("\n")
                        else:
                                print("You can't buy a negative amount!!! Nice try.")
                elif Input == "craft smelter 2" or Input == "cs2":
                        print("\n")
                        Smelter_2_logicB = []
                        Smelter_2_logicA = [Smelter_2_cost[0], Wood, Smelter_2_cost[1], Stone, Smelter_2_cost[2], Ore, Smelter_2_cost[3], Metal, Smelter_2_cost[4], Ore_type_2, Smelter_2_cost[5], Metal_type_2, Smelter_2_cost[6], Food, Ore_smelt_cost, Combined_stats_ore2]
                        for i in range(Smelter_2_cost.count(0)):
                                del Smelter_2_logicA[Smelter_2_cost.index(0) * 2]
                                del Smelter_2_logicA[Smelter_2_cost.index(0) * 2]
                        for i in range(int(len(Smelter_2_logicA) / 2)):
                                Smelter_2_logicB.append(int((Smelter_2_logicA[((i - 1) * 2) + 1] - (Smelter_2_logicA[((i - 1) * 2) + 1] % Smelter_2_logicA[((i - 1) * 2)])) / Smelter_2_logicA[((i - 1) * 2)]))
                        Smelter_2_logicB.sort()
                        print("You can buy", Fore.GREEN, Smelter_2_logicB[0], Style.RESET_ALL, "of this building.") 
                        amount = input(Fore.CYAN + "How many do you want to buy? " + Fore.MAGENTA)
                        if amount == "all":
                                print(Style.RESET_ALL + Style.BRIGHT + "Purchasing all!" + Style.RESET_ALL)
                                amount = Smelter_2_logicB[0]
                        elif amount == "half":
                                print(Style.RESET_ALL + Style.BRIGHT + "Purchasing half!" + Style.RESET_ALL)
                                amount = int(Smelter_2_logicB[0] / 2)
                        else:
                                amount = int(amount)
                        if Wood >= Smelter_2_cost[0] * amount and Stone >= Smelter_2_cost[1] * amount and Ore >= Smelter_2_cost[2] * amount and Metal >= Smelter_2_cost[3] * amount and Ore_type_2 >= Smelter_2_cost[4] * amount and Metal_type_2 >= Smelter_2_cost[5] * amount and Food >= Smelter_2_cost[6] * amount and amount >= 0 and Combined_stats_ore2 >= Ore_smelt_cost * amount:
                                Wood -= (Smelter_2_cost[0] * amount)
                                Stone -= (Smelter_2_cost[1] * amount)
                                Ore -= (Smelter_2_cost[2] * amount)
                                Metal -= (Smelter_2_cost[3] * amount) 
                                Ore_type_2 -= (Smelter_2_cost[4] * amount) 
                                Metal_type_2 -= (Smelter_2_cost[5] * amount)
                                Food -= (Smelter_2_cost[6] * amount)
                                Smelter_2_owned += (1 * amount)
                                print("\n")
                                print("Purchased ", amount, "Smelters!")
                                print("\n")
                        elif amount >= 0 and Combined_stats_ore2 >= Ore_smelt_cost * amount: 
                                print("\n")
                                print("Not enough Rescources!!! You need: Wood:", Smelter_2_cost[0] * amount, ", Stone:", Smelter_2_cost[1] * amount, ", Ore:", Smelter_2_cost[2] * amount, ", Metal:", Smelter_2_cost[3] * amount, ", Ore type 2:", Smelter_2_cost[4] * amount, ", Metal type 2:", Smelter_2_cost[5] * amount, ", Food:", Smelter_2_cost[6] * amount, "to buy ", amount, "of those!!!")
                                print("\n")
                        elif amount >= 0 and Combined_stats_ore2 < Ore_smelt_cost * amount:
                                print("\n")
                                print("You aren't producing enough ore type 2 to supply that many smelters!")
                                print("\n")
                        else:
                                print("You can't buy a negative amount!!! Nice try.")
                else:
                        print("\n")
                        print(Fore.RED, "Invalid commmand or argument!", Style.RESET_ALL)
                        print("\n")
                if Metal_type_2 >= Goal:
                        print("\n", Fore.GREEN)
                        print("You beat this level!!!")
                        print("Your total scores for this level are:")
                        print("\n")
                        print("Production stats: Wood: +", Combined_stats_wood, ", Stone: +", Combined_stats_stone, ", Ore: +", Combined_stats_ore, ", Metal: +", Combined_stats_metal, ", Ore 2: +", Combined_stats_ore2, ", Metal 2: +", Combined_stats_metal2, ", Food: +", Combined_stats_food)
                        print("\n")
                        print("Beat by day", Round, ", totaling with:    Wood:", Wood, ",  Stone:", Stone, ",  Ore:", Ore, ",  Metal:", Metal, ",  Ore type 2:", Ore_type_2, ",  Metal type 2:", Metal_type_2, ",  Food:", Food)
                        print("You owned", Location_1_owned, "x Building 1,", Location_2_owned, "x Building 2,", Location_3_owned, "x Building 3,", Location_4_owned, "x Building 4, and", Location_5_owned, "x Building 5!")
                        print("GG! Let's see if you can do that again! *evil laughter in unicode noises*", Style.RESET_ALL)
                        Goal += (((Goal / 5) + Time_Goal) + Round)
                        Goal = int(Goal)
                        Time_Goal += Round + (Time_Goal / 4) + 100
                        Time_Goal = int(Time_Goal)
                        Wood = 0
                        Stone = 0
                        Ore = 0
                        Metal = 0
                        Ore_type_2 = 0
                        Metal_type_2 = 0
                        Food = 0
                        Location_1_owned = 0
                        Location_2_owned = 0
                        Location_3_owned = 0
                        Location_4_owned = 0
                        Location_5_owned = 0
                        Smelter_owned = 0
                        Smelter_2_owned = 0
                        Round = 0
                if Round >= Time_Goal:
                        print(Fore.RED, "Uh oh! You've run out of time! Better luck next time!")
                        print("Your total scores are:")
                        print("\n")
                        print("Production stats: Wood: +", Combined_stats_wood, ", Stone: +", Combined_stats_stone, ", Ore: +", Combined_stats_ore, ", Metal: +", Combined_stats_metal, ", Ore 2: +", Combined_stats_ore2, ", Metal 2: +", Combined_stats_metal2, ", Food: +", Combined_stats_food)
                        print("\n")
                        print("Lost by day ", Round, ", totaling with:    Wood:", Wood, ",  Stone:", Stone, ",  Ore:", Ore, ",  Metal:", Metal, ",  Ore type 2:", Ore_type_2, ",  Metal type 2:", Metal_type_2, ",  Food:", Food)
                        print("You owned", Location_1_owned, "x Building 1,", Location_2_owned, "x Building 2,", Location_3_owned, "x Building 3,", Location_4_owned, "x Building 4, and", Location_5_owned, "x Building 5!")
                        print("Better luck next time...", Style.RESET_ALL)
                        Input = input(Fore.CYAN + "Continue on this diffculty, or stop? Type \"stop\" to stop, or \"continue\" to try again" + Fore.MAGENTA)
                        if Input == "stop":
                                break
                        elif Input == "continue":
                                Wood = 0
                                Stone = 0
                                Ore = 0
                                Metal = 0
                                Ore_type_2 = 0
                                Metal_type_2 = 0
                                Food = 0
                                Location_1_owned = 0
                                Location_2_owned = 0
                                Location_3_owned = 0
                                Location_4_owned = 0
                                Location_5_owned = 0
                                Round = 0
        except ValueError:
                print("\n")
                print(Fore.RED, "Invalid commmand or argument!", Style.RESET_ALL)
                print("\n")
        except EOFError:
                print("\n")
                print(Fore.RED, "Invalid commmand or argument!", Style.RESET_ALL)
                print("\n")
        except TypeError:
                print("\n")
                print(Fore.RED, "Invalid commmand or argument!", Style.RESET_ALL)
                print("\n")
        except NameError:
                print("\n")
                print(Fore.RED, "Invalid commmand or argument!", Style.RESET_ALL)
                print("\n")
